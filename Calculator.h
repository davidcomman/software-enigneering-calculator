//
// Created by Gebruiker on 04/10/2018.
//

#ifndef CALCULATOR_CALCULATOR_H
#define CALCULATOR_CALCULATOR_H

class Calculator {
public:
    int add(int a, int b);

    double add(double a, double b);

    int sub(int a, int b);

    double sub(double a, double b);

    int mul(int a, int b);

    double mul(double a, double b);

    int div(int a, int b);

    double div(double a, double b);

    int square(int num);

    double square(double num);
};


#endif //CALCULATOR_CALCULATOR_H
