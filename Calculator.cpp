//
// Created by Gebruiker on 04/10/2018.
//

#include "Calculator.h"
#include <stdexcept>
#define EPSILON .001
int Calculator::add(int a, int b) {
    return a + b;
}

double Calculator::add(double a, double b) {
    return a + b;
}

int Calculator::sub(int a, int b) {
    return a - b;
}

double Calculator::sub(double a, double b) {
    return a - b;
}

int Calculator::mul(int a, int b) {
    return a * b;
}

double Calculator::mul(double a, double b) {
    return a * b;
}

int Calculator::div(int a, int b) {
    if (b == 0) throw std::logic_error("Division by 0");
    return a / b;
}

double Calculator::div(double a, double b) {
    if (std::abs(b) < EPSILON) throw std::logic_error("Division by 0");
    return a / b;
}

int Calculator::square(int num) {
    return num*num;
}

double Calculator::square(double num) {
    return num*num;
}