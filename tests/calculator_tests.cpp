//
// Created by Gebruiker on 04/10/2018.
//

#include <gtest/gtest.h>
#include "../Calculator.h"
#include "../Calculator.cpp"

Calculator calculator;

TEST(calculator_tests, add_int) {
    int res1 = calculator.add(3, 3);
    int res2 = calculator.add(8, -5);
    int res3 = calculator.add(0, 7);
    ASSERT_EQ(res1, 6);
    ASSERT_EQ(res2, 3);
    ASSERT_EQ(res3, 7);
}

TEST(calculator_tests, add_double) {
    double res1 = calculator.add(5.8, 4.0);
    double res2 = calculator.add(2.7, -4.0);
    double res3 = calculator.add(26.8, 0.0);
    ASSERT_DOUBLE_EQ(res1, 9.8);
    ASSERT_DOUBLE_EQ(res2, -1.3);
    ASSERT_DOUBLE_EQ(res3, 26.8);
}

TEST(calculator_tests, sub_int) {
    int res1 = calculator.sub(67, 2465);
    int res2 = calculator.sub(8, -5);
    int res3 = calculator.sub(-68, -19);
    ASSERT_EQ(res1, -2398);
    ASSERT_EQ(res2, 13);
    ASSERT_EQ(res3, -49);
}

TEST(calculator_tests, sub_double) {
    double res1 = calculator.sub(1896.41, 856.10);
    double res2 = calculator.sub(2.7, -4.0);
    double res3 = calculator.sub(-3.3, -4.0);
    ASSERT_DOUBLE_EQ(res1, 1040.31);
    ASSERT_DOUBLE_EQ(res2, 6.7);
    ASSERT_DOUBLE_EQ(res3, 0.7);
}

TEST(calculator_tests, mul_int) {
    int res1 = calculator.mul(0, 2465);
    int res2 = calculator.mul(8, -5);
    int res3 = calculator.mul(-20, -5);
    ASSERT_EQ(res1, 0);
    ASSERT_EQ(res2, -40);
    ASSERT_EQ(res3, 100);
}

TEST(calculator_tests, mul_double) {
    double res1 = calculator.mul(1896.41, 0.0);
    double res2 = calculator.mul(-2.7, -4.0);
    double res3 = calculator.mul(85.9, -4.0);
    ASSERT_DOUBLE_EQ(res1, 0);
    ASSERT_DOUBLE_EQ(res2, 10.8);
    ASSERT_DOUBLE_EQ(res3, -343.6);
}

TEST(calculator_tests, div_int) {
    int res1 = calculator.div(0, 5);
    int res2 = calculator.div(-10, 2);
    int res3 = calculator.div(-32, -3);
    ASSERT_EQ(res1, 0);
    ASSERT_EQ(res2, -5);
    ASSERT_EQ(res3, 10);
    ASSERT_THROW(calculator.div(8, 0), std::logic_error);
}

TEST(calculator_tests, div_double) {
    double res1 = calculator.div(0.0, 30.0);
    double res2 = calculator.div(-2.7, -4.0);
    double res3 = calculator.div(-3.0, -1.5);
    ASSERT_DOUBLE_EQ(res1, 0);
    ASSERT_DOUBLE_EQ(res2, .675);
    ASSERT_DOUBLE_EQ(res3, 2);
    ASSERT_THROW(calculator.div(3.8, 0.0), std::logic_error);
}

TEST(calculator_tests, square_int) {
    int res1 = calculator.square(5);
    int res2 = calculator.square(-3);
    int res3 = calculator.square(0);
    ASSERT_EQ(res1, 25);
    ASSERT_EQ(res2, 9);
    ASSERT_EQ(res3, 0);
}

TEST(calculator_tests, square_double) {
    double res1 = calculator.square(0.0);
    double res2 = calculator.square(15.4);
    double res3 = calculator.square(-7.8);
    ASSERT_DOUBLE_EQ(res1, 0);
    ASSERT_DOUBLE_EQ(res2, 237.16);
    ASSERT_DOUBLE_EQ(res3, 60.84);
}

